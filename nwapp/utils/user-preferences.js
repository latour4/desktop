'use strict';

const path = require('path');
const fs = require('fs-extra');
const debounce = require('debounce');
const Store = require('jfs');
const log = require('loglevel');
const manifest = require('../package.json');
const events = require('./custom-events');
const SOUNDS = require('./sounds');

function isBool(val) {
  return typeof val === 'boolean';
}

const shallowObserver = function (obj, fn) {
  return new Proxy(obj, {
    set(target, name, val) {
      const oldValue = target[name];
      target[name] = val;
      fn([
        {
          name,
          object: target,
          type: 'update',
          oldValue,
        },
      ]);
      return true;
    },
  });
};

const DEFAULT_PREFERENCES = {
  showInMacMenuBar: {
    value: true,
    validate: isBool,
  },
  launchOnStartup: {
    value: true,
    validate: isBool,
  },
  launchHidden: {
    value: false,
    validate: isBool,
  },
  next: {
    value: false,
    validate: isBool,
  },
  showNotifications: {
    value: true,
    validate: isBool,
  },
  showNotificationsForDesktopErrors: {
    value: true,
    validate: isBool,
  },
  notificationSound: {
    value: 1,
    validate(val) {
      return val >= 0 && val <= SOUNDS.length;
    },
  },
};

const preferencesFileName = 'gitter_preferences.json';
const preferencesPath = path.join(nw.App.dataPath, preferencesFileName);
const legacyPreferencesPathMatches = nw.App.dataPath.match(new RegExp(`.*?${manifest.name}`));
const legacyPreferencesPath = path.join(
  legacyPreferencesPathMatches && legacyPreferencesPathMatches[0],
  preferencesFileName,
);

const db = new Store(preferencesPath, { pretty: true }); // FIXME: pretty Boolean - should be environment dependent

// initial load is done synchronously
let preferences = db.getSync('preferences');

// eslint-disable-next-line consistent-return
function savePreferences(changes = []) {
  log.info('Persisting preferences', { ...preferences });
  // save preferences everytime a change is made
  db.save('preferences', preferences, (err) => {
    // emit an event which indicates failure or success
    if (err) {
      log.error('ERROR: Could not save preferences.');
      return events.emit('preferences:failed', err);
    }

    log.info('Preferences saved', { ...preferences });
    events.emit('preferences:saved');
  });

  // in case any component is interested on a particular setting change
  changes.forEach((change) => {
    events.emit(`preferences:change:${change.name}`, change.object[change.name]);
  });

  events.emit('preferences:change');
}

// Migrate preferences from 3.x over to 4.x
// FIXME: Remove after January 2017
if (Object.keys(preferences).length === 0 && legacyPreferencesPath) {
  try {
    log.info('Migrating preferences from legacy 3.x');
    fs.copySync(legacyPreferencesPath, preferencesPath);
    // Grab the new preferences
    preferences = db.getSync('settings');
    // Blat the token because we want people to sign in again with the new OAuth client ID
    preferences.token = null;

    // Remove the previous
    db.delete('settings');

    savePreferences();
  } catch (err) {
    // ignore
    log.error('Failed to migrate preferences from legacy 3.x', err);
  }
}

// setting up observable
preferences = shallowObserver(
  preferences,
  debounce((changes) => {
    savePreferences(changes);
  }, 200),
);

// performs a check to guarantee health of preferences
Object.keys(DEFAULT_PREFERENCES).forEach(function (key) {
  // eslint-disable-next-line no-prototype-builtins
  if (!preferences.hasOwnProperty(key)) {
    preferences[key] = DEFAULT_PREFERENCES[key].value;
  }

  const isValid = DEFAULT_PREFERENCES[key].validate(preferences[key]);

  if (!isValid) {
    log.warn(`ERROR: Invalid setting:${key}. restoring to default`);
    preferences[key] = DEFAULT_PREFERENCES[key].value;
  }
});

log.info('Using preferences:', { ...preferences });

module.exports = preferences;
